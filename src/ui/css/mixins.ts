import { StyleSheet, Styles } from "jss";
import { createStyleSheet } from "./jss";

export const makeMixin = <
  P extends any[],
  V extends string | number | symbol,
  C extends V
>(
  generateStyles: (...params: P) => Styles<V>,
  key: C,
) => {
  const cssCache: Record<string, StyleSheet<V>> = {};

  return (...params: P) => {
    const cacheKey = JSON.stringify(params);

    let css = cssCache[cacheKey];

    if (!css) {
      const styles = generateStyles(...params);
      css = createStyleSheet(styles);
      cssCache[cacheKey] = css;
    }

    return css.classes[key];
  };
};

export type Measurement = string | number;

type WidthOptions = { marginAuto?: boolean };
export const width = makeMixin((width: Measurement, { marginAuto }: WidthOptions = {}) => ({
  width: {
    maxWidth: width,
    margin: marginAuto ? "auto" : undefined,
  },
}), "width");

type PaddingOptions = { x?: Measurement, y?: Measurement };
export const padding = makeMixin(({ x, y }: PaddingOptions) => ({
  padding: {
    paddingTop: y,
    paddingBottom: y,
    paddingLeft: x,
    paddingRight: x,
  },
}), "padding");

export const margin = makeMixin(({ x, y }: PaddingOptions) => ({
  margin: {
    marginTop: y,
    marginBottom: y,
    marginLeft: x,
    marginRight: x,
  },
}), "margin");
