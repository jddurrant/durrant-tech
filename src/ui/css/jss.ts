import { Styles, create } from "jss";
import preset from "jss-preset-default";
import { registry } from "./registry";

export const jss = create(preset());

export const createStyleSheet = <S extends string | number | symbol>(styles: Styles<S>) => {
  const css = jss.createStyleSheet(styles);
  registry.add(css);
  return css;
};
