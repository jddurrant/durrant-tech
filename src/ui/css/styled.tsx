import clsx, { ClassValue } from "clsx";
import { Styles } from "jss";
import { createStyleSheet, jss } from "./jss";
import { registry } from "./registry";

const TAG_NAMES = [
  "a",
  "abbr",
  "address",
  "area",
  "article",
  "aside",
  "audio",
  "b",
  "base",
  "bdi",
  "bdo",
  "big",
  "blockquote",
  "body",
  "br",
  "button",
  "canvas",
  "caption",
  "center",
  "cite",
  "code",
  "col",
  "colgroup",
  "data",
  "datalist",
  "dd",
  "del",
  "details",
  "dfn",
  "dialog",
  "div",
  "dl",
  "dt",
  "em",
  "embed",
  "fieldset",
  "figcaption",
  "figure",
  "footer",
  "form",
  "h1",
  "h2",
  "h3",
  "h4",
  "h5",
  "h6",
  "head",
  "header",
  "hgroup",
  "hr",
  "html",
  "i",
  "iframe",
  "img",
  "input",
  "ins",
  "kbd",
  "keygen",
  "label",
  "legend",
  "li",
  "link",
  "main",
  "map",
  "mark",
  "menu",
  "menuitem",
  "meta",
  "meter",
  "nav",
  "noindex",
  "noscript",
  "object",
  "ol",
  "optgroup",
  "option",
  "output",
  "p",
  "param",
  "picture",
  "pre",
  "progress",
  "q",
  "rp",
  "rt",
  "ruby",
  "s",
  "samp",
  "search",
  "slot",
  "script",
  "section",
  "select",
  "small",
  "source",
  "span",
  "strong",
  "style",
  "sub",
  "summary",
  "sup",
  "table",
  "template",
  "tbody",
  "td",
  "textarea",
  "tfoot",
  "th",
  "thead",
  "time",
  "title",
  "tr",
  "track",
  "u",
  "ul",
  "var",
  "video",
  "wbr",
  "webview",
] as const;

type TagName = typeof TAG_NAMES[number];

/**
 * Custom implementation of `styled`.
 *
 * @param Element React component or HTML tag name.
 * @param styles JSS styles.
 * @param baseVariant Key of base variant from `styles`.
 * @returns An element with the base styles applied, and a `variant` prop for other styles.
 */
export const styled = <S extends string | number | symbol, C extends S>(
  Element: React.ComponentType | TagName,
  styles: Styles<S>,
  baseVariant: C,
) => {
  const css = createStyleSheet(styles);

  type Props = React.ComponentProps<typeof Element> & {
    variant?: Exclude<S, C>;
    className?: ClassValue;
  };

  const TypedElement = Element as React.ComponentType<Props>;

  const Component: React.FC<Props> = ({ variant, className, ...props }) => (
    <TypedElement
      className={clsx(
        css.classes[baseVariant],
        variant && css.classes[variant],
        className,
      )}
      {...props}
    />
  );

  return Component;
};
