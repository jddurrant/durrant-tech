import { BlogEntryMetadata } from "../../../models/data";
import { margin, padding } from "../../css/mixins";
import { BlogEntryListing } from "./BlogEntryListing";
import { VisualContainer } from "./VisualContainer";

export type BlogEntriesListProps = {
  blogEntries: BlogEntryMetadata[];
};

export const BlogEntriesList: React.FC<BlogEntriesListProps> = ({ blogEntries }) => (
  <VisualContainer className={[margin({ x: "-0.5rem" }), padding({ x: 0, y: 0 })]}>
    {blogEntries.map((blogEntry) => (
      <BlogEntryListing blogEntry={blogEntry} key={blogEntry.slug} />
    ))}
  </VisualContainer>
);
