import { SITE_TITLE } from "../../../constants";
import { Column, gap } from "../layout/flex/Flex";
import { styled } from "../../css/styled";
import { margin } from "../../css/mixins";

const Footer = styled("footer", {
  footer: {
    textAlign: "center",
  },
}, "footer");

const Nav = styled("nav", {
  nav: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    gap: "0.5rem",
  },
}, "nav");

export const SiteFooter: React.FC = () => (
  <Footer>
    <Column className={gap("0.5rem")}>
      <Nav>
        <a href="/">{SITE_TITLE}</a>
        <a href="/blog">Blog</a>
      </Nav>
      <p className={margin({ x: 0, y: 0 })}>
        Copyright © 2024 Jack Durrant. All rights reserved.
      </p>
    </Column>
  </Footer>
);
