import { grey } from "@mui/material/colors";
import { BlogEntryMetadata } from "../../../models/data";
import { padding } from "../../css/mixins";
import { styled } from "../../css/styled";
import { Column, Row, alignItems, gap, justifyContent } from "../layout/flex/Flex";
import { TagDisplay } from "./TagDisplay";

export type BlogEntryListingProps = {
  blogEntry: BlogEntryMetadata;
};

const BlogEntryListingContainer = styled("div", {
  container: {
    borderBottom: `1px solid ${grey[800]}`,
    "&:last-child": {
      borderBottom: "none",
    },
  },
}, "container");

const Link = styled("a", {
  link: {
    fontSize: "1.125rem",
    color: `${grey[800]} !important`,
  },
}, "link");

export const BlogEntryListing: React.FC<BlogEntryListingProps> = ({ blogEntry }) => (
  <BlogEntryListingContainer className={padding({ x: "0.5rem", y: "0.75rem" })}>
    <Column className={gap("0.5rem")}>
      <Row>
        <Link href={`/blog/${blogEntry.slug}`}>
          {blogEntry.title}
        </Link>
      </Row>
      {!!blogEntry.tags?.length && (
        <Row className={[justifyContent("space-between"), alignItems("flex-end")]}>
          <Row className={gap("0.25rem")}>
            {blogEntry.tags.map((tag) => (
              <TagDisplay tag={tag} key={tag as any} />
            ))}
          </Row>
          {`Published ${blogEntry.publishedAt}`}
        </Row>
      )}
    </Column>
  </BlogEntryListingContainer>
);
