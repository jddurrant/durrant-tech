import { green, grey } from "@mui/material/colors";
import { Tag } from "../../../models/data";
import { createStyleSheet } from "../../css/jss";

export type TagDisplayProps = {
  tag: Tag;
};

const css = createStyleSheet({
  tagDisplay: {
    border: `1px solid ${green[500]}`,
    borderRadius: 4,
    padding: "0.125rem 0.25rem",
    fontSize: "0.8125rem",
    color: grey[800],
  },
});

export const TagDisplay: React.FC<TagDisplayProps> = ({ tag }) => (
  <div className={css.classes.tagDisplay}>
    {tag}
  </div>
);
