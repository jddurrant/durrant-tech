import { common, green, grey, yellow } from "@mui/material/colors";
import { styled } from "../../css/styled";

export const VisualContainer = styled("div", {
  visualContainer: {
    backgroundColor: yellow[100],
    color: common.black,
    "& a": {
      color: green[900],
    },
    borderRadius: 8,
    padding: "0.75rem 1rem 0",
  },
  main: {
    backgroundColor: yellow[50],
    borderRadius: 2,
    padding: "1.5rem 1.5rem 0",
  },
  primary: {},
  secondary: {
    backgroundColor: grey[900],
    color: common.white,
    "& a": {
      color: green[500],
    },
  },
}, "visualContainer");
