import { JssStyle } from "jss";
import { PropsWithChildren } from "react";
import { Measurement, makeMixin } from "../../../css/mixins";
import { styled } from "../../../css/styled";

export type FlexProps = PropsWithChildren<{
  direction?: "row" | "column";
  withPadding?: boolean;
}>;

export const Flex = styled("div", {
  flexbox: {
    display: "flex",
  },
}, "flexbox");

export const Row = styled(Flex, {
  row: {
    flexDirection: "row",
  },
}, "row");

export const Column = styled(Flex, {
  column: {
    flexDirection: "column",
  },
}, "column");

export const justifyContent = makeMixin((value: JssStyle["justifyContent"]) => ({
  justifyContent: {
    justifyContent: value as any,
  },
}), "justifyContent");

export const alignItems = makeMixin((value: JssStyle["alignItems"]) => ({
  alignItems: {
    alignItems: value as any,
  },
}), "alignItems");

export const gap = makeMixin((flexGap: Measurement) => ({
  gap: {
    gap: flexGap,
  },
}), "gap");
