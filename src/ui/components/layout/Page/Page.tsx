import clsx from "clsx";
import { PropsWithChildren } from "react";
import { padding, width } from "../../../css/mixins";
import { registry } from "../../../css/registry";
import { gap } from "../flex/Flex";
import { fonts } from "./stylesheets/fonts";
import { formatting } from "./stylesheets/formatting";
import { global } from "./stylesheets/global";
import { theme } from "./stylesheets/theme";

registry.add(fonts);
registry.add(formatting);
registry.add(global);
registry.add(theme);

export type PageProps = PropsWithChildren<{
  title: string;
}>;

export const Page: React.FC<PageProps> = ({ title, children }) => (
  <html lang="en">
    <head>
      <meta charSet="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>
        {title}
      </title>
      <link rel="stylesheet" type="text/css" href="/style.css" />
    </head>
    <body>
      {children}
    </body>
  </html>
);

export const MAIN_COLUMN_STYLES = clsx(
  width(720, { marginAuto: true }),
  padding({ y: "1rem" }),
  gap("0.5rem"),
);
