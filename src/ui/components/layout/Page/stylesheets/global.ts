import { jss } from "../../../../css/jss";

export const global = jss.createStyleSheet({
  "@global": {
    body: {
      margin: 0,
      padding: 0,
    },
  },
});
