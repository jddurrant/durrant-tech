import { common, green } from "@mui/material/colors";
import { jss } from "../../../../css/jss";

export const theme = jss.createStyleSheet({
  "@global": {
    body: {
      backgroundColor: common.black,
      color: common.white,
      fontFamily: '"Atkinson Hyperlegible", sans-serif',
    },
    a: {
      color: green[500],
    },
  },
});
