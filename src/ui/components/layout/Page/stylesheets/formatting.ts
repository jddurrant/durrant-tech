import { grey } from "@mui/material/colors";
import { jss } from "../../../../css/jss";

export const formatting = jss.createStyleSheet({
  "@global": {
    "h1, h2, h3, h4, h5, h6": {
      marginTop: "2rem",
      marginBottom: "1rem",
      "&:first-child": {
        marginTop: 0,
      },
    },
    "h1, h2": {
      fontWeight: 400,
    },
    "h3, h4, h5, h6": {
      fontWeight: 700,
    },
    h1: {
      fontSize: "2.5rem",
    },
    h2: {
      fontSize: "2rem",
    },
    hr: {
      width: "100%",
      border: "none",
      borderBottom: `1px solid ${grey[900]}`,
    },
    "ol, ul": {
      paddingLeft: "0.5rem",
    },
    "ul > li": {
      listStyleType: "none",
      "&::before": {
        content: '"-"',
        paddingRight: "0.5rem",
      },
    },
    blockquote: {
      marginLeft: "0.5rem",
      "&::before": {
        content: '">"',
        float: "left",
        marginRight: "0.5rem",
      },
    },
  },
});
