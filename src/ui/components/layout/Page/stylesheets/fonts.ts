import { jss } from "../../../../css/jss";

const prepareSrc = (sources: [url: string, format: string][]) => sources
  .map(([url, format]) => `url('${url}') format('${format}')`)
  .join(",");

export const fonts = jss.createStyleSheet({
  "@font-face": [
    {
      fontFamily: 'Atkinson Hyperlegible',
      fontWeight: 400,
      fontStyle: "normal",
      src: prepareSrc([
        ["/res/fonts/atkinson-hyperlegible/web/woff2/Atkinson-Hyperlegible-Regular-102a.woff2", "woff2"],
        ["/res/fonts/atkinson-hyperlegible/web/woff/Atkinson-Hyperlegible-Regular-102.woff", "woff"],
      ]),
    },
    {
      fontFamily: 'Atkinson Hyperlegible',
      fontWeight: 700,
      fontStyle: "normal",
      src: prepareSrc([
        ['/res/fonts/atkinson-hyperlegible/web/woff2/Atkinson-Hyperlegible-Bold-102a.woff2', 'woff2'],
        ['/res/fonts/atkinson-hyperlegible/web/woff/Atkinson-Hyperlegible-Bold-102.woff', 'woff'],
      ]),
    },
    {
      fontFamily: 'Atkinson Hyperlegible',
      fontWeight: 400,
      fontStyle: "italic",
      src: prepareSrc([
        ['/res/fonts/atkinson-hyperlegible/web/woff2/Atkinson-Hyperlegible-Italic-102a.woff2', 'woff2'],
        ['/res/fonts/atkinson-hyperlegible/web/woff/Atkinson-Hyperlegible-Italic-102.woff', 'woff'],
      ]),
    },
    {
      fontFamily: 'Atkinson Hyperlegible',
      fontWeight: 700,
      fontStyle: "italic",
      src: prepareSrc([
        ['/res/fonts/atkinson-hyperlegible/web/woff2/Atkinson-Hyperlegible-BoldItalic-102a.woff2', 'woff2'],
        ['/res/fonts/atkinson-hyperlegible/web/woff/Atkinson-Hyperlegible-BoldItalic-102.woff', 'woff'],
      ]),
    },
  ],
});
