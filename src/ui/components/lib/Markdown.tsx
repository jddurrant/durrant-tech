import { ComponentProps, Suspense, lazy } from "react";
import { styled } from "../../css/styled";

const LazyMarkdown = lazy(() => import("react-markdown"));

export type MarkdownProps = ComponentProps<typeof LazyMarkdown>;

type Components = MarkdownProps["components"];

const Image = styled("img", {
  img: {
    maxWidth: "100%",
  },
}, "img");

const components: Components = {
  img: (props) => <Image {...props} />,
};

export const Markdown: React.FC<MarkdownProps> = (props) => (
  <Suspense>
    <LazyMarkdown {...props} components={components} />
  </Suspense>
);
