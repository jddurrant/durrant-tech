import { SITE_TITLE } from "../../../constants";
import { MAIN_COLUMN_STYLES, Page } from "../layout/Page";
import { Column } from "../layout/flex/Flex";
import { SiteFooter } from "../widgets/SiteFooter";
import { VisualContainer } from "../widgets/VisualContainer";

export type ErrorProps = {
  statusCode: number;
};

const identifyError = (statusCode: number) => {
  switch (statusCode) {
    case 404:
      return {
        title: "Not Found",
        description: "The page you're looking for doesn't exist.",
      };
    default:
      return {
        title: "Unknown Error",
        description: "An unknown error occurred.",
      };
  }
};

export const Error: React.FC<ErrorProps> = ({ statusCode }) => {
  const error = identifyError(statusCode);

  return (
    <Page title={`${error.title} | ${SITE_TITLE}`}>
      <Column className={MAIN_COLUMN_STYLES}>
        <VisualContainer variant="secondary">
          <h1>{error.title}</h1>
          <p>{error.description}</p>
        </VisualContainer>
        <SiteFooter />
      </Column>
    </Page>
  );
};
