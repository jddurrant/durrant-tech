import { SITE_TITLE } from "../../../constants";
import { BlogEntryMetadata, Link } from "../../../models/data";
import { margin } from "../../css/mixins";
import { MAIN_COLUMN_STYLES, Page } from "../layout/Page";
import { Column, Row, gap } from "../layout/flex/Flex";
import { BlogEntriesList } from "../widgets/BlogEntriesList";
import { SiteFooter } from "../widgets/SiteFooter";
import { VisualContainer } from "../widgets/VisualContainer";

export type HomeProps = {
  blogEntries: BlogEntryMetadata[];
  blogEntriesCount: number;
  links: Link[];
};

export const Home: React.FC<HomeProps> = ({ blogEntries, blogEntriesCount, links }) => (
  <Page title={SITE_TITLE}>
    <Column className={MAIN_COLUMN_STYLES}>
      <VisualContainer variant="secondary">
        <h1 className={margin({ y: 0 })}>
          {SITE_TITLE}
        </h1>
        <h2 className={margin({ y: "1rem" })}>Blog</h2>
        <Column className={gap("0.5rem")}>
          <BlogEntriesList blogEntries={blogEntries} />
          {blogEntriesCount > blogEntries.length && (
            <Row>
              <a href="/blog">
                View more &raquo;
              </a>
            </Row>
          )}
        </Column>
        <h2 className={margin({ y: "1rem" })}>Links</h2>
        <ul>
          {links.map((link) => (
            <li key={link.href}>
              <a href={link.href}>
                {link.text}
              </a>
            </li>
          ))}
        </ul>
      </VisualContainer>
      <SiteFooter />
    </Column>
  </Page>
);
