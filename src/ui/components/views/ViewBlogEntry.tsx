import { SITE_TITLE } from "../../../constants";
import { BlogEntry } from "../../../models/fs";
import { MAIN_COLUMN_STYLES, Page } from "../layout/Page";
import { Column } from "../layout/flex/Flex";
import { Markdown } from "../lib/Markdown";
import { SiteFooter } from "../widgets/SiteFooter";
import { VisualContainer } from "../widgets/VisualContainer";

export type BlogEntryProps = {
  blogEntry: BlogEntry;
};

export const ViewBlogEntry: React.FC<BlogEntryProps> = ({ blogEntry }) => (
  <Page title={`${blogEntry.title} | ${SITE_TITLE}`}>
    <Column className={MAIN_COLUMN_STYLES}>
      <VisualContainer variant="main">
        <Markdown>
          {blogEntry.body}
        </Markdown>
      </VisualContainer>
      <SiteFooter />
    </Column>
  </Page>
);
