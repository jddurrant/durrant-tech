import { SITE_TITLE } from "../../../constants";
import { BlogEntryMetadata } from "../../../models/data";
import { padding } from "../../css/mixins";
import { MAIN_COLUMN_STYLES, Page } from "../layout/Page";
import { Column } from "../layout/flex/Flex";
import { BlogEntriesList } from "../widgets/BlogEntriesList";
import { SiteFooter } from "../widgets/SiteFooter";
import { VisualContainer } from "../widgets/VisualContainer";

export type BlogPageProps = {
  blogEntries: BlogEntryMetadata[];
};

export const BlogPage: React.FC<BlogPageProps> = ({ blogEntries }) => (
  <Page title={`Blog | ${SITE_TITLE}`}>
    <Column className={MAIN_COLUMN_STYLES}>
      <VisualContainer variant="secondary" className={padding({ x: "1rem", y: "0.75rem" })}>
        <h1>Blog</h1>
        <BlogEntriesList blogEntries={blogEntries} />
      </VisualContainer>
      <SiteFooter />
    </Column>
  </Page>
);
