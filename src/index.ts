import "dotenv/config";

import express, { RequestHandler } from "express";
import router from "./routes";
import { CONTENT_DIR } from "./constants";
import { getRedirects } from "./models/data";
import { createProxyMiddleware } from "http-proxy-middleware";

const createRedirectMiddleware = (to: string): RequestHandler => (req, res) => {
  res.redirect(to);
};

(async () => {
  const app = express();

  app.use(express.static(process.env.PUBLIC_DIR!, {
    maxAge: "1 day",
  }));

  app.use(express.static(`${CONTENT_DIR}/public`, {
    maxAge: "1 day",
  }));

  if (process.env.NODE_ENV === "development") {
    app.use("/res", express.static(`${__dirname}/res`));
  }

  const redirects = await getRedirects();

  redirects.forEach(({ from, to, proxy }) => {
    if (proxy) {
      const redirect = createProxyMiddleware({
        target: to,
        changeOrigin: true,
      });

      app.use(from, redirect);
    } else {
      const redirect = createRedirectMiddleware(to);
      app.use(from, redirect);
    }
  });

  app.use(router);

  const PORT = process.env.PORT || 3000;

  app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
  });
})();