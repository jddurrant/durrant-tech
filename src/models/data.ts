import fs from "fs/promises";
import { CONTENT_DIR } from "../constants";

export type Tag = string[];

export type BlogEntryMetadata = {
  title: string;
  slug: string;
  publishedAt: string;
  updatedAt?: string;
  tags?: Tag[];
};

export type Link = {
  text: string;
  href: string;
};

export type Redirect = {
  from: string;
  to: string;
  proxy?: boolean;
};

export type Data = {
  blog: BlogEntryMetadata[];
  links: Link[];
  redirects: Redirect[];
};

const readData = async (): Promise<Data> => {
  const buffer = await fs.readFile(`${CONTENT_DIR}/data.json`);
  const string = buffer.toString();
  const data: Data = JSON.parse(string);

  return data;
};

export const getTotalBlogEntries = async () => {
  const { blog } = await readData();
  return blog.length;
};

export const getBlogEntriesList = async (qty = 10, skip = 0) => {
  const { blog } = await readData();
  return blog.slice(skip, skip + qty);
};

export const getLinks = async () => {
  const { links } = await readData();
  return links;
};

export const getRedirects = async () => {
  const { redirects } = await readData();
  return redirects;
};
