import fs from "fs/promises";
import { extractTitle } from "../utils/md";
import { CONTENT_DIR } from "../constants";

export type BlogEntry = {
  slug: string;
  title: string;
  body: string;
};

export const BLOG_DIR = `${CONTENT_DIR}/blog/public`;
export const DRAFTS_DIR = `${CONTENT_DIR}/blog/drafts`;

export const getBlogEntry = async (slug: string, dir = BLOG_DIR): Promise<BlogEntry> => {
  const buffer = await fs.readFile(`${dir}/${slug}.md`);
  const string = buffer.toString();

  return {
    title: extractTitle(string),
    slug,
    body: string,
  };
};
