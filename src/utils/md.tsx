export const extractTitle = (markdown: string) => {
  const firstLine = markdown.split("\n")[0];

  if (firstLine.startsWith("# ")) {
    return firstLine.slice(2);
  } else {
    return firstLine;
  }
};
