import { Router } from "express";
import { renderToPipeableStream } from "react-dom/server";
import { getBlogEntriesList, getLinks, getTotalBlogEntries } from "./models/data";
import { DRAFTS_DIR, getBlogEntry } from "./models/fs";
import { BlogPage } from "./ui/components/views/BlogPage";
import { Error } from "./ui/components/views/Error";
import { Home } from "./ui/components/views/Home";
import { ViewBlogEntry } from "./ui/components/views/ViewBlogEntry";
import { registry } from "./ui/css/registry";

const router = Router();

router.get("/", async (req, res) => {
  const entries = await getBlogEntriesList(3);
  const qty = await getTotalBlogEntries();
  const links = await getLinks();

  renderToPipeableStream((
    <Home blogEntries={entries} blogEntriesCount={qty} links={links} />
  )).pipe(res);
});

router.get("/blog", async (req, res) => {
  const entries = await getBlogEntriesList();

  renderToPipeableStream(
    <BlogPage blogEntries={entries} />
  ).pipe(res);
});

router.get("/blog/:slug", async (req, res, next) => {
  try {
    const entry = await getBlogEntry(req.params.slug);

    const { pipe } = renderToPipeableStream((
      <ViewBlogEntry blogEntry={entry} />
    ), {
      onAllReady: () => pipe(res.contentType(".html")),
    });
  } catch (e) {
    next();
  }
});

if (process.env.NODE_ENV === "development") {
  router.get("/drafts/:slug", async (req, res, next) => {
    try {
      const entry = await getBlogEntry(req.params.slug, DRAFTS_DIR);

      renderToPipeableStream((
        <ViewBlogEntry blogEntry={entry} />
      )).pipe(res);
    } catch (e) {
      next();
    }
  });
}

router.get("/style.css", (req, res) => {
  const string = registry.toString({
    format: process.env.NODE_ENV === "development",
  });
  res.type("text/css").send(string);
});

router.all("*", (req, res) => renderToPipeableStream((
  <Error statusCode={404} />
)).pipe(res.status(404)));

export default router;
